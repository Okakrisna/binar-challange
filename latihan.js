// #1 Gunakan metode untuk membalikan isi array
//! sudah selesai
let fruits = ["Apple", "Banana", "Papaya", "Grape", "Cherry", "Peach"];
fruits.reverse();
console.log(fruits);

/* 
Output: 
Peach
Cherry
Grape
Papaya
Banana
Apple
*/

// #2 Gunakan metode untuk menyisipkan elemen baru dalam array

// ! sudah selesai
let month = [
  "January",
  "February",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

month.shift(0);
month.shift(1);
month.unshift("January", "February", "March", "April", "mei", "june");
console.log(month);

/* 
Output: 
 ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
*/

// #3 Mengambil beberapa kata terakhir dalam string
//! sudah selesai
const message = "Sampaikan pada Sabrina belajar Javascript sangat menyenangkan";

const mengambilData = message.substring(23, 41);
const mengambilData2 = message.substring(49);

console.log(mengambilData, mengambilData2);
/* 
Output: belajar Javascript menyenangkan
*/

// #4 Buat agar awal kalimat jadi kapital
//! sudah selesai
const message2 =
  "Sampaikan pada Sabrina belajar Javascript sangat menyenangkan";

const mengambilData3 =
  message2.charAt(23).toUpperCase() + message2.substring(24, 41);
const mengambilData4 = message2.substring(49);
console.log(mengambilData3, mengambilData4);
/* 
Output: Belajar Javascript menyenangkan
*/

/* #5 Buat function untuk menghitung BMI (Body Mass Index)
- BMI = mass / height ** 2 = mass / (height * height) (mass in kg and height in meter)
- Steven weights 78 kg and is 1.69 m tall. Bill weights 92 kg and is 1.95 
m tall
*/

//#6 Menghitung BMI dengan if else statement
//- John weights 95 kg and is 1.88 m tall. Nash weights 85 kg and is 1.76
//m tall

let countHmi = [
  { name: "jhon", weight: 95, tall: 1.85 },
  { name: "nash", weight: 85, tall: 1.76 },
];

const bmi = 95 / 1.88 ** 2;

if (weight > 85 && tall > 1.76) {
  console.log("berat");
} else {
}

// Output example:
// John's BMI (28.3) is higher than Nash's (23.5)

//  #7 Looping
//! sudah selesai
let data = [10, 20, 30, 40, 50];
let total = 0;

for (let i = 0; i < data.length; i++) {
  total += data[i];
}

/* You code here (you are allowed to reassigned the variable) 
Maybe you can write 3 lines or more
Use for, do while, while for, or forEach
*/

console.log(`Jumlah total = ${total}`);

/* 
Jumlah total = 150
*/
